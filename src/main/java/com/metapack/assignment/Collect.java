package com.metapack.assignment;

import java.util.Arrays;

public class Collect implements Accumulator {

	private static int total=0;
	private int sum;
	
	
	
	public Collect() {
		this.reset();
	}

	public int getSum() {
		return sum;
	}

	public int accumulate(int... values) { 	  
 	  sum=Arrays.stream(values).reduce(0,(x,y)->Math.addExact(x, y));
	   total=total+this.sum;
	    	 return sum;
	}

	public int getTotal() {
		return total;
	}

	public void reset() {
       total=0;
	}

}
