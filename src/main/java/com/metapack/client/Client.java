package com.metapack.client;

import java.util.Arrays;
import java.util.Scanner;

import com.metapack.assignment.Accumulator;
import com.metapack.assignment.Collect;



public class Client {

	public static void main(String args[]) {
		Accumulator acc=new Collect();

		Scanner scan = new Scanner(System.in);
		System.out.println("****enter a number for number of times you want to call accumulator****");
		while (!scan.hasNextInt()) 
		{        
			scan.next(); // Read and discard offending non-int input
			System.out.print("Please enter an integer: "); // Re-prompt
		}
		int num=scan.nextInt();
		for(int i=0;i<num;i++) {

			System.out.println("enter numbers to add up with space in between ");
			Scanner scan1 = new Scanner(System.in);
			String st=scan1.nextLine();
			
			if(st.trim().length()>0) {

				if(!st.matches("[\\-*\\d+\\s]*")){
					System.out.println(" please enter numbers with space in between and number of times you can use accumulator "+ (num=num-1));
					continue;
				}

				String[] values= st.split(" ");
				int[] intValues=Arrays.asList(values).stream().mapToInt(n-> Integer.parseInt(n)).toArray();
				acc.accumulate(intValues);
				System.out.println("Total "+acc.getTotal());
				if(i!=num-1) {
					System.out.println(" Enter 0 to reset ,* to exit and any key to continue ");
					String key=scan1.next();
					if(key.equals("0")) {
						acc.reset();
					}else if(key.equals("*")) {
						break;
					}
				}
			}else {
				System.out.println("You have hit enter without entering the numbers");
			}
		}
		System.out.println("Thankyou");

	}

}
