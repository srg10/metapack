package com.metapack.assignment;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class AccumulatorTest {

	Accumulator collect1;
	int[] values = { 1, 2, 3, 4 };
	int[] values2 = { 1, 2, 3 };


	@Before
	public void setup() {
		collect1= new Collect();
	}


	@Test
	public void testAccumulateWithOneAccumulator() {
		int sum=collect1.accumulate(values);
		assertEquals(10, sum);

	}
	/*this test failed when total was calculated in getTotal and had to move calcuation of total to accumulate*/

	@Test
	public void testMultipleAccumultaeWithSameInstances() {
		collect1.accumulate(values);
		assertEquals(10, collect1.getTotal());
		collect1.accumulate(values2);
		int values3[]= {41};
		collect1.accumulate(values3);
		assertEquals(57, collect1.getTotal());

	}


	/*
	 * this is to test getTotal without calling getTotal after calling accumulator
	 * on int[] values
	 */
	@Test
	public void testTotalAccumultaeWithSameInstances() {

		collect1.accumulate(values);
		collect1.accumulate(values2);
		assertEquals(16, collect1.getTotal());

	}

	/* test to check the value of total in collect across multiple instances*/
	@Test
	public void testAccumulateWithMultipleCollectInstances() {

		collect1.accumulate(values);
		Accumulator collect2 = new Collect();
		collect2.accumulate(values2);
		assertEquals(6, collect2.getTotal());

	}
	@Test
	public void testAccumulateafterReset() {

		collect1.accumulate(values);
		collect1.reset();
		collect1.accumulate(values2);
		collect1.accumulate(values2);
		assertEquals(12, collect1.getTotal());

	}


	/*this test is to check the value of total in collect across multiple instances if reset is removed from the constuctor otherwise
	 * if reset is present while creating instance  to pass the testcases have metioned the expected error .*/
	@Test(expected=AssertionError.class)
	public void test2AccumulateWithMultipleCollectInstances() {
		collect1.reset();
		collect1.accumulate(values);
		Accumulator collect2 = new Collect();
		collect2.accumulate(values2);
		assertEquals(20, collect2.getTotal());		

	}


	@Test(expected=ArithmeticException.class)
	public void testWrapAround() {

		int[] values= {2147483647,1};
		collect1.accumulate(values);

	}
}



